import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OperadorFrontOfficeComponent } from './modules/operador-front-office/operador-front-office.component';

const routes: Routes = [
  {
    path: 'front-office',
    component: OperadorFrontOfficeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
