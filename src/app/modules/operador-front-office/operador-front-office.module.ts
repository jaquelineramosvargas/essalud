import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperadorFrontOfficeComponent } from './operador-front-office.component';



@NgModule({
  declarations: [
    OperadorFrontOfficeComponent
  ],
  imports: [
    CommonModule
  ]
})
export class OperadorFrontOfficeModule { }
